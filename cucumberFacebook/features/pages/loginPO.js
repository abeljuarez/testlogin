(function() {
  'use strict';
  
var PropertiesReader = require('properties-reader');
var or = PropertiesReader('./features/properties/or.properties');
var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
var expect = chai.expect;


  var PageObject = require('./ComunMethodsImp.js');

  var elements = {
    username: element(by.id(or.get('username_id'))),
    password: element(by.id(or.get('password_id'))),
    registrarse: element(by.xpath(or.get('registrarse_xpath'))),
    loginbtn: element(by.id(or.get('loginbtn_id')))
  };

  var ContactPage = new PageObject(elements);
  module.exports = ContactPage;
})();
