var PropertiesReader = require('properties-reader');
var or = PropertiesReader('./features/properties/or.properties');
var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
var expect = chai.expect;
/**
 * Provides convenience methods for your e2e tests
 *
 * @param {string}  baseUrl     The base path to be used by page.get and page.getUrl
 * @param {*[]}     elements    A collection of elements like this: { myButton: element(by.id('myBtn')), ...}
 *                              This parameter is optional and is kept to support old code.
 *
 * @example
 *  (function() {
 *      var PageObject = require('support/page-object');
 *      var baseUrl = '/mypage';
 *      var elements = {
 *          editButton: element(by.id('edit-btn')),
 *          cancelButton: element(by.id('cancel-btn'))
 *      };
 *      var MyPage = new PageObject(baseUrl, elements);
 *      module.exports = MyPage;
 *  })();
 */
//module.exports = function (baseUrl, elements) {
 module.exports = function (elements) {
  'use strict';
  var flow = browser.controlFlow();
  var page = this;

  //page.baseUrl = baseUrl;
  page.elements = elements;

  /* * * * * * * * * * * * *
   * PROMISE / FLOW METHODS
   * * * * * * * * * * * * */

  /**
   * Execute a sequence of promises sequentially
   * @param {*[]} actionPromises
   * @returns {Promise}
   */
  page.executeSequence = function(actionPromises) {
      return protractor.promise.all(
          actionPromises.map(function(promise) {
              return flow.execute(function() { return promise; });
          })
      );
  };

  /* * * * * * * * * * * * *
   * PAGE RELATED METHODS
   * * * * * * * * * * * * */

  /**
   * Opens the page.baseUrl
   * @returns {Promise}
   */
  //page.get = function() {
    // return page.getUrl('');
  //};


  /* * * * * * * * * * * * * * *
   * ELEMENT INTERACTION METHODS
   * * * * * * * * * * * * * * */

  /**
   * Click an element (uses browser.actions to avoid an IE bug)
   * @param {WebElement} element
   * @returns {Promise}
   */
  page.go= function(site) {

    return page.executeSequence([
        browser.waitForAngularEnabled(false),
        browser.get(site),
       
        ]);
    
},


 page.getTitle= function(){
    return page.executeSequence([
        browser.getTitle(),
        browser.sleep(1000)
        ]);
     
},


  page.click = async function(element) {
      return page.executeSequence([
     await browser.actions().click(element).perform(),
        browser.sleep(1000)
        ]);
     
  };  

  page.dradAndD = async function(el1, el2) {
    return page.executeSequence([
        await  browser.actions().dragAndDrop(el1, el2).perform(),
      browser.sleep(1000)
      ]);
   
};  
 

  
  page.getContent =  function(element) {
    return element.getAttribute('value')
      .then(function (val) {
            return (val === null) ?
               element.getText() :
                val;
        });
    

  };
  
  page.fill = function (element, value) {
    return page.executeSequence([
        element.clear(),
        element.sendKeys(value)
      //  browser.sleep(1000)
     ]);
    
  };

  page.selectDropdownItemByValue = function(dropdown, value) {
    return page.executeSequence([
        page.click(dropdown),
        dropdown.all(by.css('option[value="' + value + '"]')).click(),
       // browser.sleep(1000)
    ]);
};


page.selectDropdownItemByText = function(dropdown, text) {
  return page.executeSequence([
      page.click(dropdown),
      dropdown.all(by.xpath('//li[text()="'+ text +'"]')).click(),
      browser.sleep(1000)
  ]);
};

page.inicioSesion = async function(usuario, password, valueUsuario, valuePassword, btnISesion) {
  return page.executeSequence([
    usuario.sendKeys(valueUsuario),
    password.sendKeys(valuePassword),
    password.sendKeys(protractor.Key.TAB),
    global.textbotonlogin = await btnISesion.getText(),
    await browser.actions().click(btnISesion).perform(),
    browser.sleep(1000)
  ]);
};
page.cerrarSesion = async function(dropSesion, logout) {
  return page.executeSequence([
    browser.sleep(1000),
     browser.actions().click(dropSesion).perform(),
    browser.sleep(1000),
     browser.actions().click(logout).perform(),
    browser.sleep(1000)
  ]);
};


};