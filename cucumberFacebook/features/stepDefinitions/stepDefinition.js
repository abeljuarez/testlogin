var {Given,When,Then,Before,After,setDefaultTimeout} = require('cucumber');
setDefaultTimeout(60 * 1000);
var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
var expect = chai.expect;
var PropertiesReader = require('properties-reader');
var or = PropertiesReader('./features/properties/or.properties');
const fs = require('fs');

'use strict';
var base = require('../pages/BasePage.js'); 
var loginP = require('../pages/loginPO.js'); 



Before(function(){
	browser.ignoreSynchronization=true;
	browser.driver.manage().window().maximize();

//	const filename1 = (process.cwd() + '\\e2e\\download\\For-SIN-013.zip');

});

After(function(scenarioResult) {
   let self = this;
 
   if(scenarioResult.result.status === 'failed'){
   return browser.takeScreenshot()
   .then(function (screenshot) {
	   const decodedImage = new Buffer(screenshot.replace(/^data:image\/png;base64,/, ''), 'base64');
	   self.attach(decodedImage, 'image/png');
	   console.log("scenarioResult  = ",scenarioResult.result.status);
   });
}


});

  
	Given(/^Inicio Sesión en la url "([^"]*)"$/, async function (url) {
	  
		base.go(url);
		browser.sleep(3000);	
		
		var actualTitle = base.getTitle();
		return expect(actualTitle).to.eventually.equal("Facebook - Entrar o registrarse");
		
	});

	
	When(/^inicio sesion con credenciales "([^"]*)" y "([^"]*)"$/, async function (usuario,pass) {
			
		loginP.fill(loginP.elements.username,usuario);
		loginP.fill(loginP.elements.password,pass);
	
	//});				
	
		return expect(await loginP.getContent(loginP.elements.registrarse)).to.equal("Registrarte");
});

