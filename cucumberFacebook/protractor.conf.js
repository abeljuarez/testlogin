var PropertiesReader = require('properties-reader');
var prop = PropertiesReader('./features/properties/prop.properties');

var filePath = (process.cwd() + '\\e2e\\download\\');


exports.config = {
    seleniumAddress: prop.get('seleniumAddress'),
    //directConnect: true,
    getPageTimeout: prop.get('pageTimeOut'),
    allScriptsTimeout: prop.get('scriptTimeOut'),
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
  
    capabilities:{

      "browserName": 'chrome',
      'chromeOptions': {
        'args': ['incognito'],
        prefs: {
            download: {
                'prompt_for_download': false,
                'directory_upgrade': true,
                'default_directory': filePath
            }
        }
    }
    },

   
    plugins: [{
      package: require.resolve('protractor-multiple-cucumber-html-reporter-plugin'),
      options:{
        automaticallyGenerateReport: true,
        removeExistingJsonReportFile: true,
        reportName: 'QA ILM',
        pageFooter: '<div><p>QA Automation ILM</p></div>',
        pageTitle: 'Execution Report	',
        customData:{
          title: 'Execution info',
          data:[

            {label: 'Project', value: 'ILM'},
            {label: 'Release', value: '1.2'},
            {label: 'Tester', value: 'Abel Juárez Presa'}

          ]


        },

        displayDuration: true
      }
  }],

    maxSessions: 2,

    specs: [prop.get('featurePath')],

    cucumberOpts: {
      // require step definitions
      tags: false,
      format: ['html:target/cucumber', 'json:target/cucumber.json'],
      require: [
        prop.get('stepsPath') // accepts a glob
      ]
    }
    
  };